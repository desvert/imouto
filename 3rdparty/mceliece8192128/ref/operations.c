#include "api.h"

#include "controlbits.h"
#include "encrypt.h"
#include "decrypt.h"
#include "params.h"
#include "sk_gen.h"
#include "pk_gen.h"
#include "util.h"

#include <stdint.h>
#include <string.h>

#include "../../../chacha20.h"
#include "../../sphincsplus/ref/sha256.h"
#define crypto_hash_32b sha256

void
crypto_kem_enc (unsigned char c[MCELIECE_CIPHERTEXTBYTES],
		unsigned char key[MCELIECE_BYTES],
		const unsigned char pk[MCELIECE_PUBLICKEYBYTES])
{
  unsigned char two_e[ 1 + SYS_N/8 ] = {2};
  unsigned char *e = two_e + 1;
  unsigned char one_ec[ 1 + SYS_N/8 + (SYND_BYTES + 32) ] = {1};

  //

  encrypt(c, pk, e);

  crypto_hash_32b(c + SYND_BYTES, two_e, sizeof(two_e));

  memcpy(one_ec + 1, e, SYS_N/8);
  memcpy(one_ec + 1 + SYS_N/8, c, SYND_BYTES + 32);

  crypto_hash_32b(key, one_ec, sizeof(one_ec));
}

void
crypto_kem_dec (unsigned char key[MCELIECE_BYTES],
		const unsigned char c[MCELIECE_CIPHERTEXTBYTES],
		const unsigned char sk[MCELIECE_SECRETKEYBYTES])
{
  int i;

  unsigned char ret_confirm = 0;
  unsigned char ret_decrypt = 0;

  uint16_t m;

  unsigned char conf[32];
  unsigned char two_e[ 1 + SYS_N/8 ] = {2};
  unsigned char *e = two_e + 1;
  unsigned char preimage[ 1 + SYS_N/8 + (SYND_BYTES + 32) ];
  unsigned char *x = preimage;

  //

  ret_decrypt = decrypt(e, sk + SYS_N/8, c);

  crypto_hash_32b(conf, two_e, sizeof(two_e));

  for (i = 0; i < 32; i++) ret_confirm |= conf[i] ^ c[SYND_BYTES + i];

  m = ret_decrypt | ret_confirm;
  m -= 1;
  m >>= 8;

  *x++ = (~m &     0) | (m &    1);
  for (i = 0; i < SYS_N/8;         i++) *x++ = (~m & sk[i]) | (m & e[i]);
  for (i = 0; i < SYND_BYTES + 32; i++) *x++ = c[i];

  crypto_hash_32b(key, preimage, sizeof(preimage));
}

static const unsigned char zero[8] = {0};

void
crypto_kem_keypair (unsigned char pk[MCELIECE_PUBLICKEYBYTES],
		    unsigned char sk[MCELIECE_SECRETKEYBYTES],
		    unsigned char seed[32])
{
  int i;
  uint32_t state[16];
  uint32_t R[ (SYS_T*2 + (1 << GFBITS)*sizeof(uint32_t) + SYS_N/8 + 32) / sizeof (uint32_t) + 1028] = {0};
  unsigned char *r = (unsigned char *)R;
  unsigned char *rp;

  gf f[ SYS_T ]; // element in GF(2^mt)
  gf irr[ SYS_T ]; // Goppa polynomial
  uint32_t perm[ 1 << GFBITS ]; // random permutation

  while (1)
    {
      rp = r;
      chacha20_mkstate (state, seed, zero, zero);
      for (size_t i = 0; i + 1024 < sizeof R / sizeof *R; i += 1024)
	chacha20_64 (R + i, state);
      memcpy(seed, r + sizeof R - 4128, 32);

      for (i = 0; i < SYS_T; i++) f[i] = load_gf(rp + i*2);
      rp += sizeof(f);
      if (genpoly_gen(irr, f)) continue;

      for (i = 0; i < (1 << GFBITS); i++) perm[i] = load4(rp + i*4);
      rp += sizeof(perm);

      for (i = 0; i < SYS_T;   i++) store_gf(sk + SYS_N/8 + i*2, irr[i]);
      if (pk_gen(pk, sk + SYS_N/8, perm)) continue;

      memcpy(sk, rp, SYS_N/8);
      controlbits(sk + SYS_N/8 + IRR_BYTES, perm);

      break;
    }
}
