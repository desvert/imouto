# ImoutoCrypt
A PGP-like software for post-quantum encryption.

If big brother is always watching.. the imouto is always hiding something from him.

# Licence
- All files on the top level are licenced under the GNU Affero General Public Licence version 3. You can find its full text in `licences/agpl-3.0.txt`.
- All files under `3rdparty/poly1305-donna` are based on poly1305-donna commit `e6ad6e091d30d7f4ec2d4f978be1fcfcbce72781` and are in the public domain.
- All files under `3rdparty/sphincsplus` are based on commit `c510ae29f4be9bcf9ff8e9f057accc4920a9338c` from the official implementation. Their licence is CC0. You can find its full text in `licences/CC0-1.0-universal.txt`.
- All files under `3rdparty/mceliece8192128` are based on `supercop-20200702` and are in the public domain.

# Alogrithms used
- SPHINCS+ 256f sha256 (`3rdparty/sphincsplus`)
- Classic McEliece 8192128 (`3rdparty/mceliece8192128`)
- Chacha20 (`chacha20.c`, `chacha20.h`)
- Poly1305 (`3rdparty/poly1305-donna`)

# Warnings
- Currently all of the input is stored in memory because SPHINCS+ requires the input to be streamed twice.
- If you are planing to send a file encrypted with imouto to someone else please make sure that you are using a protocol which protects your metadata (sadly I am not aware of any at the moment).
- If you wish to avoid storing the whole file in memory you can sign a hash of the file but be warned that this will require a collision-resistant hash function, unlike signing the file directly with sphincs+ which only requires a hash function with preimage resistance.
- I have not evaluated https://csrc.nist.gov/CSRC/media/Projects/post-quantum-cryptography/documents/round-3/official-comments/Sphincs-Plus-round3-official-comment.pdf
- This software comes without any warranty. Also please bear in mind that it is still experimental.
- Only systems with 64-bit size_t are officially supported.

# Usage
- Keygen (KEM): `imouto -t mceliece -o enc.priv > enc.pub`
- Keygen (Sign): `imouto -t sphincs+ -o sig.priv > sig.pub`
- Encryption: `imouto enc.pub enc2.pub ... < input > output`
- Signing and encrypting: `imouto -s sign.priv enc.pub enc2.pub ... < input > output`
- Decryption: `imouto -d enc.priv enc2.priv ... < input > output`
- Decryption and verification: `imouto -d -s sign.pub enc.priv enc2.priv ... < input > output`
- Signing: `imouto -s sign.priv < input > output`
- Verification: `imouto -d -s sign.pub < input > output`
- Signing (detached): `imouto -D -s sign.priv < input`
- Verification (detached): `imouto -dD -s sign.pub sig < input`

# Notes
- You can find my signing public key in this repository named `imouto.pub`
- Imouto in Japanese means younger sister.

# Design
- All the implementations of the algorithms used within imouto are constant-time.
- Sign-then-encrypt is used (with the signature being encrypted as well), in addition to that the unencrypted headers and the key exchanges are signed. See https://crypto.stackexchange.com/questions/5458/should-we-sign-then-encrypt-or-encrypt-then-sign
- Encrypted files consist of a 64-bit field with the value `"imoutoF\x00"`, a 2nd 64-bit field with the number of key exchanges, the key exchanges (which consist of the mceliece ciphertext, a 256-bit nonce, and a 256-bit key encrypted with xchacha20 using the mceliece sharedkey and the nonce), and the encrypted data (this includes 3 64-bit headers, the value `"imoutoS\x00"` if no signing is used and `"imoutoS\x01"` if signing is used, the length of the plaintext, and the number of encrypted blocks). Each block of encrypted data is 4080-octets long, the first 4064 octets contain the encrypted data while the rest 16 octets contain the poly1305 mac.

# TODO
- Do not store the whole file in memory when only encryption is used.
- Allow double-streaming a file stored in disk rather than store it in memory (will require minor changes to the sphincs+ implementation).
- FUSE integration for mounting files.
- GUI frontent, likely in Qt.
- Padding (until it gets to a specific size or random)
- Secret key encryption
- Add the option to select different implementations of the algorithms at compile time (and the ability to benchmark them)
