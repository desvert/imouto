/* ImoutoCrypt: Post-quantum encryption tool
 * Copyright (C) 2020  The ImoutoCrypt creator
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>

#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>

#include <fcntl.h>

#include <stdnoreturn.h> /* because gcc a bad */

#include "3rdparty/mceliece8192128/ref/api.h"
#include "3rdparty/mceliece8192128/ref/operations.h"

#include "3rdparty/poly1305-donna/poly1305-donna.h"

#include "3rdparty/sphincsplus/ref/api.h"

#include "chacha20.h"

#define MIN(x, y) ((((x) <= (y)) * (x)) ^ (((x) > (y)) * (y)))

noreturn void
error (const char *fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  vfprintf (stderr, fmt, args);
  va_end (args);
  exit (EXIT_FAILURE);
}

bool
readfile (const char *path, size_t size, unsigned char out[size])
{
  FILE *f = fopen (path, "r");
  bool ret;

  if (!f)
    error ("Could not open file ``%s'': %m\n", path);

  if (!fread (out, size, 1, f))
    {
      if (feof (f))
	error ("``%s'' is too small\n", path);
      else
	error ("Error while reading ``%s'': %m\n", path);
    }
  ret = (getc (f) != EOF || !feof (f));

  if (fclose (f))
    error ("Error while closing ``%s'': %m\n", path);

  return ret;
}

void *
xrealloc (void *p, size_t newsize)
{
  p = realloc (p, newsize);
  if (!p)
    error ("Allocation failed: %m\n");
  return p;
}

void
doKE (unsigned char ke[MCELIECE_CIPHERTEXTBYTES + 64], const char *path, const unsigned char key[32])
{
  unsigned char nonce[32];
  unsigned char sharedkey[32];
  uint32_t out[16];
  unsigned char pubkey[MCELIECE_PUBLICKEYBYTES + 8];

  readfile (path, sizeof pubkey, pubkey);

  if (!memcmp (pubkey, "imoutoK\x00", 8))
    {
      getentropy (nonce, sizeof nonce);
      memcpy (out, key, 32);
      crypto_kem_enc (ke, sharedkey, pubkey + 8);

      /* the final + is not needed since we use only 256 bits but whatever */
      xchacha20 (out, sharedkey, nonce + 24, nonce);

      memcpy (ke + MCELIECE_CIPHERTEXTBYTES + sizeof nonce, out, 32);
      explicit_bzero (out, sizeof out);

      memcpy (ke + MCELIECE_CIPHERTEXTBYTES, nonce, sizeof nonce);
    }
  else
    error ("Unknown file format for enc public key ``%s''\n", path);
}

bool
readall (unsigned char **plaintext, size_t *plaintext_size)
{
  while (1) {
    *plaintext = xrealloc (*plaintext, *plaintext_size + 4096);
    *plaintext_size += fread (*plaintext + *plaintext_size, 1, 4096, stdin);
    if (feof (stdin))
      return true;
    else if (ferror (stdin))
      return false;
  }
}

static const unsigned char zero[8] = {0};

int
main (int argc, char *argv[])
{
  int opt;

  const size_t sphincssize = (SPHINCS_BYTES / 4064 + !!(SPHINCS_BYTES % 4064)) * 4064;
  const char *type = 0;
  const char *privkeyout = 0;
  const char *sign = 0;
  bool decrypt = false;
  bool detached = false;

  /* the key to encrypt the plaintext with */
  unsigned char key[32];

  /* chacha20 state */
  uint32_t state[16];

  unsigned char *plaintext = 0;
  size_t plaintext_size = 0;

  setrlimit (RLIMIT_CORE, &(struct rlimit) {0, 0});
  mlockall (MCL_CURRENT | MCL_FUTURE);
  while ((opt = getopt (argc, argv, ":s:t:o:dD")) != -1)
    switch (opt)
      {
      case 's':
	if (sign)
	  error ("Only a single signature is supported\n");
	sign = optarg;
	break;
      case 'D':
	detached = true;
	break;
      case 'd':
	decrypt = true;
	break;
      case 't':
	type = optarg;
	break;
      case 'o':
	privkeyout = optarg;
	break;
      case ':':
	error ("-%c requires a value\n", optopt);
      case '?':
	error ("unknown argument %c\n", optopt);
      }

  if (type)
    {
      int fd;

      if (decrypt || sign || argv[optind] || detached)
	error ("Can't generate a keypair and encrypt/decrypt at the same time\n");
      else if (!privkeyout)
	error ("Missing a private key output path (with -o)\n");

      fd = open (privkeyout, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
      if (fd == -1)
	error ("Failed to open/create ``%s'': %m\n", privkeyout);

      if (!strcmp (type, "sphincs+"))
	{
	  unsigned char pk[SPHINCS_PUBLICKEYBYTES + 8] = "imoutoK\x02";
	  unsigned char sk[SPHINCS_SECRETKEYBYTES + 8] = "imoutoK\x03";
	  unsigned char seed[SPHINCS_SEEDBYTES];
	  getentropy (seed, sizeof seed);
	  crypto_sign_seed_keypair (pk + 8, sk + 8, seed);
	  if (write (fd, sk, sizeof sk) != sizeof sk)
	    error ("Failed to write the private key at ``%s'': %m\n", privkeyout);
	  if (close (fd))
	    error ("Failed to close the private key file at ``%s'': %m\n", privkeyout);
	  if (!fwrite (pk, sizeof pk, 1, stdout))
	    error ("Failed to write the public key: %m\n");
	}
      else if (!strcmp (type, "mceliece"))
	{
	  unsigned char pk[MCELIECE_PUBLICKEYBYTES + 8] = "imoutoK\x00";
	  unsigned char sk[MCELIECE_SECRETKEYBYTES + 8] = "imoutoK\x01";
	  unsigned char seed[32];
	  getentropy (seed, sizeof seed);
	  crypto_kem_keypair (pk + 8, sk + 8, seed);
	  if (write (fd, sk, sizeof sk) != sizeof sk)
	    error ("Failed to write the private key at ``%s'': %m\n", privkeyout);
	  if (close (fd))
	    error ("Failed to close the private key file at ``%s'': %m\n", privkeyout);
	  if (!fwrite (pk, sizeof pk, 1, stdout))
	    error ("Failed to write the public key: %m\n");
	}
      else
	error ("Unknown file type: %s\n", type);
    }
  else if (privkeyout)
    error ("-o can't be used without -t\n");
  else if ((decrypt && sign && !argv[optind] && !detached)
	   || (decrypt && sign && detached))
    {
      FILE *f = stdin;
      unsigned char pub[SPHINCS_PUBLICKEYBYTES + 8];
      unsigned char sig[SPHINCS_BYTES];
      uint64_t len;

      if (detached)
	{
	  unsigned char h[8];

	  if (!argv[optind])
	    error ("Expected a detached signature\n");

	  f = fopen (argv[optind], "r");

	  if (!fread (h, 8, 1, f))
	    error ("Read error or ill-formatted file (during the reading of the signature header)");

	  if (memcmp (h, "imoutoF\x02", 8))
	    error ("Invalid format for the detached signature\n");

	  if (!readall (&plaintext, &plaintext_size))
	    error ("Input error: %m\n");
	}
      else
	{
	  plaintext = xrealloc (plaintext, 16);
	  if (!fread (plaintext, 8, 1, stdin))
	    error ("Could not read the 1st header of stdin (read error or ill-formatted file): %m\n");

	  if (!memcmp (plaintext, "imoutoF\x00", 8))
	    error ("This is an encrypted file, please supply a private key to decrypt it\n");
	  else if (!memcmp (plaintext, "imoutoF\x01", 8))
	    ;
	  else if (!memcmp (plaintext, "imoutoF\x02", 8))
	    ;
	  else
	    error ("Unknown file format on input\n");

	  if (!fread (plaintext + 8, 8, 1, stdin))
	    error ("Could not read the 2nd header of stdin (read error or ill-formatted file): %m\n");

	  memcpy (&len, plaintext + 8, 8);

	  if (len + 16 < len)
	    error ("Ill-formatted file: length field is too big (overflow)\n");

	  plaintext = xrealloc (plaintext, 16 + len);
	  if (!fread (plaintext + 16, len, 1, stdin))
	    error ("Read error or ill-formatted file (during the reading of the plaintext): %m\n");
	  plaintext_size = 16 + len;
	}

      if (!fread (sig, sizeof sig, 1, f))
	error ("Read error or ill-formatted file (during the reading of the signature): %m\n");

      readfile (sign, sizeof pub, pub);

      if (memcmp (pub, "imoutoK\x02", 8))
	error ("Unknown file format for signing public key ``%s''\n", sign);

      if (!crypto_sign_verify (sig, plaintext, plaintext_size, pub + 8))
	error ("Invalid signature\n");

      if (detached)
	{
	  puts ("OK");
	  fclose (f);
	}
      else if (!fwrite (plaintext + 16, len, 1, stdout))
	error ("Failed to write the plaintext: %m\n");
    }
  else if (sign && !argv[optind])
    {
      unsigned char priv[SPHINCS_SECRETKEYBYTES + 8];
      unsigned char sig[SPHINCS_BYTES + 8] = "imoutoF\x02";

      if (!detached)
	{
	  plaintext = xrealloc (plaintext, 16);
	  memcpy (plaintext, "imoutoF\x01", 8);
	  plaintext_size += 16;
	}

      if (!readall (&plaintext, &plaintext_size))
	error ("Input error: %m\n");

      if (!detached)
	memcpy (plaintext + 8, (uint64_t []){plaintext_size - 16}, 8);

      readfile (sign, sizeof priv, priv);

      if (memcmp (priv, "imoutoK\x03", 8))
	error ("Unknown file format for signing private key ``%s''\n", sign);

      crypto_sign_signature (sig + 8, plaintext, plaintext_size, priv + 8);
      if (detached)
	{
	  if (!fwrite (sig, sizeof sig, 1, stdout))
	    error ("Failed to write the signature: %m\n");
	}
      else
	{
	  if (!fwrite (plaintext, plaintext_size, 1, stdout))
	    error ("Failed to write the plaintext: %m\n");
	  if (!fwrite (sig + 8, sizeof sig - 8, 1, stdout))
	    error ("Failed to write the signature: %m\n");
	}
    }
  else if (detached)
    error ("Detached is used with signing/verifying only\n");
  else if (!argv[optind])
    error ("No op\n");
  else if (decrypt)
    {
      uint32_t block[1024];
      uint64_t length;
      uint64_t blocks;
      uint64_t reallen = 4064;

      plaintext = xrealloc (plaintext, plaintext_size + 8);
      if (!fread (plaintext + plaintext_size, 8, 1, stdin))
	error ("Could not read the 1st header of stdin (read error or ill-formatted file): %m\n");
      plaintext_size += 8;

      if (!memcmp (plaintext, "imoutoF\x00", 8))
	{
	  bool run = true;
	  uint64_t kenum;
	  unsigned char *end;

	  plaintext = xrealloc (plaintext, plaintext_size + 8);
	  if (!fread (plaintext + plaintext_size, 8, 1, stdin))
	    error ("Could not read the 2nd header of stdin (read error or ill-formatted file): %m\n");
	  memcpy (&kenum, plaintext + plaintext_size, 8);
	  plaintext_size += 8;

	  plaintext = xrealloc (plaintext, plaintext_size + (MCELIECE_CIPHERTEXTBYTES + 64) * kenum + 4080);
	  end = plaintext + plaintext_size + (MCELIECE_CIPHERTEXTBYTES + 64) * kenum;
	  if (!fread (plaintext + plaintext_size, (MCELIECE_CIPHERTEXTBYTES + 64) * kenum + 4080, 1, stdin))
	    error ("Could not read the key exchanges and 1st block from stdin (read error or ill-formatted file): %m\n");

	  do {
	    unsigned char privkey[MCELIECE_SECRETKEYBYTES + 8];
	    readfile (argv[optind], sizeof privkey, privkey);
	    if (!memcmp (privkey, "imoutoK\x01", 8))
	      for (size_t i = 0; i < kenum; i++)
		{
		  unsigned char sharedkey[32];
		  unsigned char nonce[32];
		  uint32_t chacha[16];
		  unsigned char *local = plaintext + plaintext_size + i * (MCELIECE_CIPHERTEXTBYTES + 64);
		  unsigned char mac[16];
		  unsigned char key[32];

		  crypto_kem_dec (sharedkey, local, privkey + 8);
		  memcpy (nonce, local + MCELIECE_CIPHERTEXTBYTES, 32);
		  memcpy (chacha, local + MCELIECE_CIPHERTEXTBYTES + 32, 32);
		  xchacha20 (chacha, sharedkey, nonce + 24, nonce);
		  explicit_bzero (sharedkey, sizeof sharedkey);
		  memcpy (key, chacha, 32);
		  explicit_bzero (chacha, sizeof chacha);

		  chacha20_mkstate (state, key, zero, zero);
		  explicit_bzero (key, sizeof key);
		  memset (block, 0, sizeof block);
		  chacha20_64 (block, state);

		  poly1305_auth (mac, end, 4064, ((unsigned char *)block) + 4064);
		  if (poly1305_verify (mac, end + 4064))
		    {
		      run = false;
		      break;
		    }
		}
	    else
	      error ("Unknown file type for the private key\n");
	  } while (argv[++optind] && run);
	  if (run)
	    error ("Could not perform a key exchange with this/these private mceliece key(s)\n");
	  plaintext_size += (MCELIECE_CIPHERTEXTBYTES + 64) * kenum;
	}
      else
	error ("Invalid file format\n");

      for (size_t i = 0; i < 4064; i++)
	plaintext[plaintext_size + i] ^= ((unsigned char *)block)[i];

      if (!memcmp (plaintext + plaintext_size, "imoutoS\x00", 8))
	{
	  if (sign)
	    error ("Unsigned file\n");
	}
      else if (!memcmp (plaintext + plaintext_size, "imoutoS\x01", 8))
	;
      else
	error ("Unknown subversion!\n");

      memcpy (&length, plaintext + plaintext_size + 8, 8);
      memcpy (&blocks, plaintext + plaintext_size + 16, 8);

      if (!fwrite (plaintext + plaintext_size + 24, MIN (4040, length), 1, stdout))
	error ("Error writing to the output: %m\n");
      length -= MIN (4040, length);

      if (!blocks)
	error ("Ill-formatted file: blocks field is 0\n");
      else if (blocks >> 51)
	error ("Ill-formatted file: blocks field is too big (total file size would be more than 2^63 octets)\n");
      else if ((blocks << 12) < length)
	error ("Ill-formatted file: the expected plaintext is bigger than the one that can fit in the file\n");
      else if (length >> 63)
	error ("Ill-formatted file: length field is too big (total file size would be more than 2^63 octets)\n");

      if (sign)
	{
	  if ((length + 24 + SPHINCS_BYTES) > blocks * 4064)
	    error ("Block field too small for the file to fit the signature and the header\n");
	  plaintext = xrealloc (plaintext, plaintext_size + blocks * 4064);
	}

      for (uint64_t i = 1; i < blocks; i++, length -= MIN (4064, length))
	{
	  uint32_t block[1024] = {0};
	  uint32_t block2[1024] = {0};
	  unsigned char mac[16];

	  if (!fread (block2, 4080, 1, stdin))
	    error ("Read error or input too short: %m\n");

	  chacha20_64 (block, state);

	  poly1305_auth (mac, (unsigned char *)block2, 4064, ((unsigned char *)block) + 4064);

	  for (size_t i = 0; i < 4064; i++)
	    ((unsigned char *)block)[i] ^= ((unsigned char *)block2)[i];

	  if (!poly1305_verify (mac, ((unsigned char *)block2) + 4064))
	    error ("Could not verify the integrity of the ciphertext\n");

	  if (length && !fwrite (block, MIN (4064, length), 1, stdout))
	    error ("Failed to write to the output: %m\n");

	  reallen += !!length * 4064;
	  if (sign)
	    memcpy (plaintext + plaintext_size + i * 4064, block, 4064);
	}

      if (sign)
	{
	  unsigned char sigpub[SPHINCS_PUBLICKEYBYTES + 8];
	  readfile (sign, sizeof sigpub, sigpub);

	  if (memcmp (sigpub, "imoutoK\x02", 8))
	    error ("Unknown file format for signing public key ``%s''\n", sign);

	  if (!crypto_sign_verify (plaintext + plaintext_size + reallen,
				   plaintext,
				   plaintext_size + reallen,
				   sigpub + 8))
	    error ("Invalid signature\n");
	}
    }
  else
    {
      plaintext = xrealloc (plaintext, plaintext_size + 16);
      memcpy (plaintext + plaintext_size, "imoutoF\x00", 8);
      memcpy (plaintext + plaintext_size + 8, (uint64_t []){argc - optind}, 8);
      plaintext_size += 16;

      if (!fwrite (plaintext, plaintext_size, 1, stdout))
	error ("Error during the header-writing: %m\n");

      getentropy (key, sizeof key);
      do {
	unsigned char ke[MCELIECE_CIPHERTEXTBYTES + 64];

	doKE (ke, argv[optind], key);

	if (!fwrite (ke, sizeof ke, 1, stdout))
	  error ("Error while writing the key exchange: %m\n");

	if (sign)
	  {
	    plaintext = xrealloc (plaintext, plaintext_size + sizeof ke);
	    memcpy (plaintext + plaintext_size, ke, sizeof ke);
	    plaintext_size += sizeof ke;
	  }
      } while (argv[++optind]);

      const size_t plaintext_start = plaintext_size;
      size_t blocksize = 4040; /* 4096
				* - 32 (mac + mac key)
				* - 24 (version + length + blocks)
				*/
      size_t len = 0;
      size_t blocks = !!sign * (sphincssize / 4064);

      chacha20_mkstate (state, key, zero, zero);
      explicit_bzero (key, sizeof key);

      plaintext = xrealloc (plaintext, plaintext_size + 24);
      plaintext_size += 24;
      while (1)
	{
	  plaintext = xrealloc (plaintext, plaintext_size + blocksize);
	  memset (plaintext + plaintext_size, 0, blocksize);

	  len += fread (plaintext + plaintext_size, 1, blocksize, stdin);

	  plaintext_size += blocksize;
	  blocks++;
	  blocksize = 4064;

	  if (feof (stdin))
	    break;
	  else if (ferror (stdin))
	    error ("Error during plaintext reading: %m\n");
	}

      memcpy (plaintext + plaintext_start + 8, (uint64_t []){len}, 8);
      memcpy (plaintext + plaintext_start + 16, (uint64_t []){blocks}, 8);

      if (sign)
	{
	  unsigned char sigpriv[SPHINCS_SECRETKEYBYTES + 8];

	  memcpy (plaintext + plaintext_start, "imoutoS\x01", 8);

	  readfile (sign, sizeof sigpriv, sigpriv);

	  if (memcmp (sigpriv, "imoutoK\x03", 8))
	    error ("Unknown file format for signing private key ``%s''\n", sign);

	  plaintext = xrealloc (plaintext, plaintext_size + sphincssize);
	  crypto_sign_signature (plaintext + plaintext_size, plaintext, plaintext_size, sigpriv + 8);
	  memset (plaintext + plaintext_size + SPHINCS_BYTES, 0, sphincssize - SPHINCS_BYTES);
	  plaintext_size += sphincssize;
	}
      else
	memcpy (plaintext + plaintext_start, "imoutoS\x00", 8);

      for (size_t i = plaintext_start; i < plaintext_size; i += 4064)
	{
	  uint32_t chunk[1024];
	  unsigned char mackey[32];
	  memcpy (chunk, plaintext + i, 4064);
	  memset (((unsigned char *)chunk) + 4064, 0, 32);
	  chacha20_64 (chunk, state);
	  memcpy (mackey, ((unsigned char *)chunk) + 4064, 32);
	  poly1305_auth (((unsigned char *)chunk) + 4064, (unsigned char *)chunk, 4064, mackey);
	  explicit_bzero (mackey, sizeof mackey);
	  if (!fwrite (chunk, 4080, 1, stdout))
	    error ("Error during the writing of the ciphertext %m");
	}
    }
  explicit_bzero (state, sizeof state);

  if (fclose (stdout))
    error ("Failed to write the output: %m\n");

  return EXIT_SUCCESS;
}
