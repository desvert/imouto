struct poly1305_state;

extern void poly1305_init(struct poly1305_state *ctx, const unsigned char key[32]);
extern void poly1305_update(struct poly1305_state *ctx, const unsigned char *m, size_t bytes);
extern void poly1305_finish(struct poly1305_state *ctx, unsigned char mac[16]);
extern void poly1305_auth(unsigned char mac[16], const unsigned char *m, size_t bytes, const unsigned char key[32]);

extern int poly1305_verify(const unsigned char mac1[16], const unsigned char mac2[16]);
extern int poly1305_power_on_self_test(void);
