extern void crypto_kem_enc (unsigned char c[MCELIECE_CIPHERTEXTBYTES],
			    unsigned char key[MCELIECE_BYTES],
			    const unsigned char pk[MCELIECE_PUBLICKEYBYTES]);

extern void crypto_kem_dec (unsigned char key[MCELIECE_BYTES],
			    const unsigned char c[MCELIECE_CIPHERTEXTBYTES],
			    const unsigned char sk[MCELIECE_SECRETKEYBYTES]);

extern void crypto_kem_keypair (unsigned char pk[MCELIECE_PUBLICKEYBYTES],
				unsigned char sk[MCELIECE_SECRETKEYBYTES],
				unsigned char seed[32]);
