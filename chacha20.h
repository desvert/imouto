/* ImoutoCrypt: Post-quantum encryption tool
 * Copyright (C) 2020  The ImoutoCrypt creator
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

extern void chacha20_mkstate (uint32_t state[restrict 16],
			      const unsigned char key[32],
			      const unsigned char counter[8],
			      const unsigned char nonce[8]);

extern void chacha20 (uint32_t plaintext[restrict 16],
		      const uint32_t x[16]);

extern void xchacha20 (uint32_t plaintext[restrict 16],
		       const unsigned char key[32],
		       const unsigned char counter[8],
		       const unsigned char nonce[24]);

extern void chacha20_64 (uint32_t plaintext[restrict 1024],
			 uint32_t x[restrict 16]);
