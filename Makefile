# ImoutoCrypt: Post-quantum encryption tool
# Copyright (C) 2020  The ImoutoCrypt creator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CFLAGS = -Wall -Wextra -Os -ftree-vectorize -march=native -std=gnu11 -DNDEBUG=1 -D_FILE_OFFSET_BITS=64 -Wno-unused-result -Wno-maybe-uninitialized
SOURCES = main.c chacha20.c $(wildcard 3rdparty/sphincsplus/ref/*.c) 3rdparty/poly1305-donna/poly1305-donna.c $(wildcard 3rdparty/mceliece8192128/ref/*.c)
HEADERS = $(wildcard 3rdparty/sphincsplus/ref/*.h) $(wildcard 3rdparty/poly1305-donna/*.h) $(wildcard 3rdparty/mceliece8192128/ref/*.h) chacha20.h

.PHONY: all
all: imouto

imouto: $(SOURCES) $(HEADERS) Makefile
	$(CC) $(CFLAGS) -o $@ $(SOURCES)

clean:
	rm imouto
