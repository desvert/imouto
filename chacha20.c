/* ImoutoCrypt: Post-quantum encryption tool
 * Copyright (C) 2020  The ImoutoCrypt creator
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

static uint32_t
rotl (uint32_t x,
      int n)
{
  return ((x << n) ^ (x >> (32 - n)));
}

#define QR(a, b, c, d)				\
  do {						\
    a += b;					\
    d = rotl (d ^ a, 16);			\
    c += d;					\
    b = rotl (b ^ c, 12);			\
    a += b;					\
    d = rotl (d ^ a, 8);			\
    c += d;					\
    b = rotl (b ^ c, 7);			\
  } while (0)

static void
chacha20p (uint32_t x[16])
{
  int i;
  for (i = 0; i < 10; i++)
    {
      int j;
      /* column */
      for (j = 0; j < 4; j++)
	QR (x[     j],
	    x[ 4 + j],
	    x[ 8 + j],
	    x[12 + j]);
      /* diagonal */
      for (j = 0; j < 4; j++)
	QR (x[           j      ],
	    x[ 4 + ((1 + j) & 3)],
	    x[ 8 + ((2 + j) & 3)],
	    x[12 + ((3 + j) & 3)]);
    }
}

static const uint32_t constant[4] = {0x61707865, 0x3320646e, 0x79622d32, 0x6b206574};

void
chacha20_mkstate (uint32_t state[restrict 16],
		  const unsigned char key[32],
		  const unsigned char counter[8],
		  const unsigned char nonce[8])
{
  memcpy (state +  0, constant, 16);
  memcpy (state +  4, key     , 32);
  memcpy (state + 12, counter ,  8);
  memcpy (state + 14, nonce   ,  8);
}

void
chacha20 (uint32_t plaintext[restrict 16],
	  const uint32_t state[16])
{
  uint32_t tmp[16];
  int i;
  memcpy (tmp, state, sizeof tmp);
  chacha20p (tmp);
  for (i = 0; i < 16; i++)
    plaintext[i] ^= tmp[i] + state[i];
}

void
xchacha20 (uint32_t plaintext[restrict 16],
	   const unsigned char key[32],
	   const unsigned char counter[8],
	   const unsigned char nonce[24])
{
  uint32_t tmp[16];

  chacha20_mkstate (tmp, key, nonce, nonce + 8);
  chacha20p (tmp);

  memcpy (tmp + 4, tmp +  0, 16);
  memcpy (tmp + 8, tmp + 12, 16);

  memcpy (tmp +  0, constant +  0, 16);
  memcpy (tmp + 12, counter  +  0,  8);
  memcpy (tmp + 14, nonce    + 16,  8);

  chacha20 (plaintext, tmp);
}

void
chacha20_64 (uint32_t plaintext[restrict 1024],
	     uint32_t x[restrict 16])
{
  int i;
  uint64_t c = ((uint64_t)x[12] << 32) ^ x[13];
  for (i = 0; i < 64; i++)
    {
      chacha20 (plaintext + (i * 16), x);
      c++;
      x[12] = c;
      x[13] = c >> 32;
    }
}
