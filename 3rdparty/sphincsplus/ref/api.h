#ifndef SPX_API_H
#define SPX_API_H

#include <stddef.h>
#include <stdint.h>

#include "params.h"

#define SPHINCS_SECRETKEYBYTES SPX_SK_BYTES
#define SPHINCS_PUBLICKEYBYTES SPX_PK_BYTES
#define SPHINCS_BYTES SPX_BYTES
#define SPHINCS_SEEDBYTES 3*SPX_N

/*
 * Generates a SPHINCS+ key pair given a seed.
 * Format sk: [SK_SEED || SK_PRF || PUB_SEED || root]
 * Format pk: [root || PUB_SEED]
 */
extern void crypto_sign_seed_keypair (unsigned char pk[SPHINCS_PUBLICKEYBYTES], unsigned char sk[SPHINCS_SECRETKEYBYTES],
				      const unsigned char seed[SPHINCS_SEEDBYTES]);

/**
 * Returns an array containing a detached signature.
 */
extern void crypto_sign_signature (uint8_t sig[SPHINCS_BYTES],
				   const uint8_t *m, size_t mlen, const uint8_t sk[SPHINCS_SECRETKEYBYTES]);

/**
 * Verifies a detached signature and message under a given public key.
 */
extern int crypto_sign_verify (const uint8_t sig[SPHINCS_BYTES],
			       const uint8_t *m, size_t mlen, const uint8_t pk[SPHINCS_PUBLICKEYBYTES]);

#endif
